# Git config
ZSH_THEME_GIT_PROMPT_CLEAN="%F{green}"
ZSH_THEME_GIT_PROMPT_DIRTY="%F{yellow}"

ZSH_THEME_GIT_PROMPT_ADDED="%F{cyan}✈ %f"
ZSH_THEME_GIT_PROMPT_MODIFIED="%F{yellow}✱ %f"
ZSH_THEME_GIT_PROMPT_DELETED="%F{red}✗ %f"
ZSH_THEME_GIT_PROMPT_RENAMED="%F{blue}➦ %f"
ZSH_THEME_GIT_PROMPT_UNMERGED="%F{magenta}✂ %f"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%F{yellow}%B+ %b%f"

local user='%B%n%b@%U%m%u%B:%b'
local pwd='%B%~%b'
local separator='%(?..%F{red})»%f'

PROMPT='${user} ${pwd} ${separator} '
RPROMPT='$(git_prompt_status)%B$(git_repo_name)%b $(parse_git_dirty)$(git_current_branch)%f'
